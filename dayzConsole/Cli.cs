using System;
using System.IO;
using System.Diagnostics;
using System.Threading;
using Microsoft.Win32; 

namespace dayzConsole
{
	public class Cli
	{
		public Cli ()
		{
		}
		//CLI loop
		public int mainMenu() {
			Console.Clear();
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Dayz Updater Cli");
			Console.ForegroundColor = ConsoleColor.Gray;
			while(true) {
				Console.Write("> ");
				string cin = Console.ReadLine();
				cin = cin.ToLower();
				if(menuHandle(cin)) break;
			}
			return 1;
		}
		//CLI user input to define the Arma2 PATH
		public static void defineArmaPath() {
			//runs the find auto path, falls back to manual 
			if(findArmaPath()){
				Console.WriteLine("specify your Arma2 OA path. C:\\Program Files...");
				Console.Write("path: ");
				Filesystem.armaPath = Console.ReadLine();
			}

			//checks armaPath exists falls back to manual
			while(!Directory.Exists(Filesystem.armaPath)) {
				Console.Clear ();
				Console.WriteLine("specify your Arma2 OA path. C:\\Program Files...");
				Console.Write("path: ");
				Filesystem.armaPath = Console.ReadLine();
			}
			
			//assign installPath to @dayz
			Filesystem.installPath = Path.Combine(Filesystem.armaPath, "@dayz");
			//create dayz folder if it doesn't exist
			if(!Directory.Exists(Filesystem.installPath)) Directory.CreateDirectory(Filesystem.installPath);
			//assign installPath to Addons
			Filesystem.installPath = Path.Combine(Filesystem.installPath, "Addons");
			//if Addons doesn't exist, make it.
			if(!Directory.Exists(Filesystem.installPath)) Directory.CreateDirectory(Filesystem.installPath);
			
			Console.WriteLine("@dayz install path = " + Filesystem.installPath );
			Console.Write("press any key to continue . . .");
			Console.ReadKey();
			Console.Clear();
		}
		
		//checks registry for arma2 path (returns false when path is auto set)
		public static bool findArmaPath() {
			RegistryKey rk = Registry.LocalMachine;
			
			string cin; //input in
			try {
				if(IntPtr.Size == 8){ //is running 64bit mode? Might have to find a better way of determining 64bit
					rk = rk.OpenSubKey(@"SOFTWARE\Wow6432Node\Bohemia Interactive Studio\ArmA 2 OA");
				}
				else {
					rk = rk.OpenSubKey(@"SOFTWARE\Bohemia Interactive Studio\ArmA 2 OA");
				}
				//read input to check directory is correct
				do{
					Console.Clear ();
					Console.WriteLine("Arma2 path found: " + rk.GetValue("main")  + "\nIs this correct? (y/n)");
					cin = Console.ReadLine();
				}
				while(cin != "y" && cin != "n");
				if(cin == "y") {
					Filesystem.armaPath = (string)rk.GetValue("main");
					rk.Close();
					return false;
				}
				rk.Close();
				return true;
			}
			catch(Exception e) {
				Console.WriteLine(e.Message);
				Console.WriteLine("Could not auto detect Arma2 path.");
				return true;
			}
		}
		
		public static void launch() {
			Process pr = new Process();
			//check path
			if(!File.Exists(Path.Combine(Filesystem.armaPath, "_runA2CO.cmd"))) {
				Console.WriteLine("Error: _runA2CO.cmd not found, can not launch.");
				return;
			}
			
			//start proccess
			pr.StartInfo.FileName = (Path.Combine(Filesystem.armaPath, "_runA2CO.cmd"));
			pr.StartInfo.Arguments = ("-nosplash -mod=@DayZ -world=empty -skipIntro");
			pr.Start();
			
			//feedback
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.Write("Arma2 ");
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write("DayZ");
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.Write(" started.\n");
		}
		
		//for dealing with the menu
		private bool menuHandle(string cin) {
			//need to put in a check for path if someone enters this into the CLI: "> path C:\Program Files..."
			switch(cin) {
				case "help":
					this.help();
					break;
				case "?":
					this.help();
					break;
				case "update":
					update();
					break;
				case "launch":
					launch();
					break;
				case "path":
					defineArmaPath();
					break;
				//to install from existing files on the system (mainly for debugging) will make this a propper installer later
				case "install":
					Console.Clear();
					Console.ForegroundColor = ConsoleColor.Red;
					
					Filesystem.removeUncompressedDayz();
					Filesystem.uncompressAllFiles();
					
					Console.Clear();
					Console.ForegroundColor = ConsoleColor.Gray;
					Console.Beep();
				break;
				
				case "exit":
				   return true;
				case "": break;
				default:
					//Console.WriteLine("Unknown Command: " + cin);
					this.help();
					return false;
			}
			return false;
		}
		
		//output help
		private void help() {
			Console.Clear();
			Console.WriteLine("Dayz Unofficial CLI Launcher.");
			Console.WriteLine("-----------------------------");
			Console.WriteLine("help:\t\t Shows these options for the CLI\n");
			Console.WriteLine("?:\t\t see help.\n");
			Console.WriteLine("update:\t\t checks if there is an update avaliable. \n");
			Console.WriteLine("launch:\t\t launches arma2 with dayz\n");
			Console.WriteLine("path:\t\t attempts to automatically find the Arma2 path. Fall back is to manually specify path.");
			Console.WriteLine("\t\t note, it is possible to drag and drop folders into the console.\n");
			Console.WriteLine("exit:\t\t quits the launcher\n");
			Console.WriteLine("N.B All commands are case insensitive.");
		}
		
		//update method
		public static void update() {
			Console.WriteLine("Checking for updates...");
			//if there is an update
			if(ListMatch.compareLists()) {
				string cin = "p";
				//ask user if they want to update
				while(cin != "y" && cin != "n") {
					Console.Clear();
					Console.WriteLine("Update system? (y/n)");
					cin = Console.ReadLine();
					cin = cin.ToLower(); //case insensitive
				}
				//if user selects yes
				if(cin == "y") {
					Console.WriteLine("Updating system...");
					Filesystem f = new Filesystem();
					f.update();
				}
			}
		}
	}
}