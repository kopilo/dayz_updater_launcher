using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Generic;

namespace dayzConsole
{
	class ListMatch
	{
		public static List<string> newFiles = new List<string>();
		public static List<string> oldFiles = new List<string>();
		public static List<string> currentFiles = new List<string>();
		
		//COMPARE lists
		public static bool compareLists() {
			//clear lists
			//newFiles.Clear(); oldFiles.Clear(); currentFiles.Clear();
			
			//get lists to compare
			string[] localList = getLocalList().ToArray();
			string[] remoteList = getRemoteList().ToArray();
			
			var diff = remoteList.Except(localList);
			var inter = remoteList.Intersect(localList);
			var rem = localList.Except(remoteList);
			
			bool needsUpdate = false;
			
			//NEW files
            foreach (var s in diff) {
                newFiles.Add(s);
                needsUpdate = true;
            }
			
 			//CURRENT Files
            foreach (var s in inter) {  
                currentFiles.Add(s);
            }
			
 			//OLD Files
            foreach (var s in rem) {
                oldFiles.Add(s);
                needsUpdate = true;
            }
			return needsUpdate;
		}
		
		//GET local list
		private static List<string> getLocalList() {
			//create DirInfo object
			DirectoryInfo dir = new DirectoryInfo(MainClass.downloadPath);
			//grab files in dir
            FileInfo[] fir = dir.GetFiles();
			//convert array to list
            List<string> outlist = new List<string>();
            foreach (FileInfo j in fir) {
                outlist.Add(j.Name);
            }
			return outlist;
		}
		
		//GET remote list
		private static List<string> getRemoteList() {
			//pull remote list from php
			WebClient client = new WebClient();
			//using list instead of string array, split list
			string downloadString = client.DownloadString("http://polandchan.com/zombocom/latest/grab_latest_links.php");
			List<string> remoteList = new List<string>(downloadString.Split('|'));
			//gets the last index key
			int e = remoteList.Count -1;
			//grabs the last index value
			MainClass.downloadURL = remoteList[e];
			//removes last index
			remoteList.RemoveAt(e);
			return remoteList;
		}
		//Change from using the PHP script to checking if either of the download pages are up.
	}
}