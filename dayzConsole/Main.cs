using System;
using System.IO;

namespace dayzConsole
{
	class MainClass
	{
		public static string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData); //Appdata folder.
		public static string appsettings = Path.Combine(appdata, "DayzUpdaterFailEdition"); //ini settings here
    	public static string downloadPath = Path.Combine(appsettings, "compressed"); //Download to this directory.
		public static string downloadURL;
		
		//MAIN
		public static void Main(string[] args) {
			
			//configure console
			Console.Clear();
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.BackgroundColor = ConsoleColor.Black;
			Console.Title = "DayZ Unofficial Updater";
			
			//read in settings if file exists
			if(File.Exists(Path.Combine(appsettings, "preferences.ini")))
				Filesystem.readSettings(Path.Combine(appsettings, "preferences.ini"));
			
			//create path if it does not exist
			if(!(Directory.Exists(downloadPath))) {
				Console.WriteLine("Creating Dayz updater folder: " + downloadPath);
				Directory.CreateDirectory(downloadPath);
			}
			//check if there are valid args.
			if(args.Length > 0) {
				//do stuff
				parameters(args);
			}
			else {
				//if no valid args start cli
				Cli ui = new Cli();
				ui.mainMenu();
				//added in pause if CLI launch.
				Console.Write("Press any key to continue . . ."); Console.ReadKey();
			}
			//write out settings
			Filesystem.writeSettings(Path.Combine(appsettings, "preferences.ini"));
			
		}
		//to deal with cmd parameters
		private static void parameters(string[] args) {
			//for each element in args
		 	foreach(string p in args) {
				//if key=value
				if(p.IndexOf("=") != -1) {
					string[] split = p.Split('=');
					switch (split[0]) {
						//parameter path
						case "-path":
							if(Directory.Exists(split[1])) {
								Filesystem.armaPath = split[1];
								Filesystem.installPath = Path.Combine(Filesystem.armaPath, "@dayz");
								if(!Directory.Exists(Filesystem.installPath)) Directory.CreateDirectory(Filesystem.installPath);
								Filesystem.installPath = Path.Combine(Filesystem.installPath, "Addons");
								if(!Directory.Exists(Filesystem.installPath)) Directory.CreateDirectory(Filesystem.installPath);
								Console.WriteLine("Path set to: " + Filesystem.armaPath);
							}
							else {
								Console.WriteLine("Error: specified path does not exist("+split[1]+")");
							}
						break;
					}
				}
				else
				{
					switch(p) {
						//launch parameter
						case "-launch":
							Cli.launch();
						break;
						case "-update":
							Cli.update();
						break;
						default:
							coutHelp();	
						break;
					}
				}
			}
		}
		
		//command line parameters help
		private static void coutHelp() {
			Console.WriteLine("USAGE: [-help | /? | -path[arma2 path] | -update | -launch ]");
			Console.WriteLine("\n\t Options:");
			Console.WriteLine("\t\t /? \t displays this help message.");
			Console.WriteLine("\t\t -help \t displays this help message.");
			Console.WriteLine("\t\t -path \t sets the path to Arma2 Operation Arrowhead.");
			Console.WriteLine("\t\t -update \t checks for an update to DayZ.");
			Console.WriteLine("\t\t -launch \t launches ArmaCO - DayZ");
		}
	}
}