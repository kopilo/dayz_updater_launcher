using System;
using System.ComponentModel;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using SevenZip;

namespace dayzConsole
{
	public class Filesystem
	{
		private List<string> downloads = new List<string>();
		private WebClient wc = new WebClient();
		public static string installPath = "-1";
		public static string armaPath = "-1";
			
		public Filesystem () //initalisation
		{
		}
		
		//removes the old files
		public void deleteOldFiles() {
			ListMatch.newFiles.ForEach(delegate(string name) {
				Console.WriteLine("Removing old file: " + name);
				File.Delete(Path.Combine(MainClass.downloadPath, name));
			});
		}
		
		//async download initialisation
		public void downloadNewFilesAsync() {
			this.downloads = ListMatch.newFiles;
			//create handlers
			wc.DownloadFileCompleted += new AsyncCompletedEventHandler(wc_DownloadFileCompleted);
			wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(wc_ProgressChanged);
			//start download
			wc.DownloadFileAsync(new Uri(MainClass.downloadURL+'/'+this.downloads[0]), MainClass.downloadPath + '\\'+ this.downloads[0]);
		}
		
		//async download complete handle
		void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e) {
			Console.WriteLine("Download Completed: " + this.downloads[0]);
			Thread.Sleep(300);
			
    		this.downloads.RemoveAt(0); //when a download completes remove it.
			
			if(downloads.Count > 0) {
				//start next file downloading
				wc.DownloadFileAsync(new Uri(MainClass.downloadURL+'/'+this.downloads[0]), Path.Combine(MainClass.downloadPath, this.downloads[0]));
			}
			else { //download complete
				Console.WriteLine("Downloads Completed");
				uncompressAllFiles();
			}
		}
		
		//async download progress handle
		private void wc_ProgressChanged(object sender, DownloadProgressChangedEventArgs e) {
			Console.Clear();
			Console.WriteLine("Downloading " + this.downloads[0] + '('+e.ProgressPercentage +"%)");
		}
		
		//synchronous method (not used)
		public void DownloadNewFilesSync() {
			ListMatch.newFiles.ForEach(delegate(string name) {
				Console.WriteLine("Downloading update: " + name);
				WebClient wc = new WebClient();
				wc.DownloadFileAsync(new Uri(MainClass.downloadURL+'/'+name), System.IO.Path.Combine(MainClass.downloadPath, name));
			});
			uncompressNewFiles();
		}
		
		//clear old files from arma
		public static void removeUncompressedDayz() {
			//remove all files
			Array.ForEach(Directory.GetFiles(installPath),
              delegate(string path) { File.Delete(path); });
		}
		
		//uncompress new files
		public static void uncompressNewFiles() {
			//ensure install path exists
			while(!Directory.Exists(installPath)) {
				Console.WriteLine("Error: Install path does not exist("+ installPath +")");
				Cli.defineArmaPath();
				return;
			}
			//set 7z lib
			if(File.Exists("7z.dll")) SevenZip.SevenZipBase.SetLibraryPath("7z.dll");
			else {
				Console.WriteLine("ERROR binary missing: 7z.dll. Unable to decompress. Please redownload the launcher.");
				return;
			}

			//for each new file
			ListMatch.newFiles.ForEach(delegate(string name) {
				string file = Path.Combine(MainClass.downloadPath, name);
				//open the archieve with 7z.
				SevenZipExtractor archieve = new SevenZipExtractor(file);
				//check for corruption :: if corruption delete file and ask user to reupdate. 
				if(archieve.Check()) {
					Console.WriteLine("extracting2: " + name);
					//extract
					archieve.ExtractArchive(installPath);
				}
				else {
					//if file exists, delete it
					if(File.Exists(file)) File.Delete(file);
					//output error
					Console.WriteLine("ERROR: Corrupted file: " + name +"\n please run update again.");
				}
			});
			Console.Write("> ");
		}
		
		//uncompress all the files
		public static void uncompressAllFiles() {
			//ensure install path exists
			while(!Directory.Exists(installPath)) {
				Console.WriteLine("Error: Install path does not exist("+ installPath +")");
				Cli.defineArmaPath();
				return;
			}
			//set 7z lib
			if(File.Exists("7z.dll")) SevenZip.SevenZipBase.SetLibraryPath("7z.dll");
			else {
				Console.WriteLine("ERROR binary missing: 7z.dll. Unable to decompress. Please redownload the launcher.");
				return;
			}
			
			//for each file in the download folder
			Array.ForEach(Directory.GetFiles(MainClass.downloadPath), delegate (string file){
				//open archieve
				SevenZipExtractor archieve = new SevenZipExtractor(file);
				if(archieve.Check()) {
					//if not corrupt
					Console.WriteLine("Extracting: " + archieve.FileName);
					//extract
					archieve.ExtractArchive(installPath);
				}
				else { //if corrupt
					Console.WriteLine("ERROR: " + archieve.FileName + " is corrupt.");
				}
			});
			Console.Write("> ");
		}
		
		//update method
		public void update() {
			//check path is even usable..
			while(!(Directory.Exists(installPath))) {
				Console.WriteLine("Error: Install path does not exist("+ installPath +")");
				Cli.defineArmaPath();
			}
			this.deleteOldFiles();
			this.downloadNewFilesAsync();
		}
		
		//write setting
		public static void writeSettings(string file) {
			if(!(Directory.Exists(installPath))) return; //if no install path set do nothing.
			//check if file exists
			if(!File.Exists(file)) {
				Console.WriteLine("creating ini file: " + file);
			}
			//write crap to file (overrights)
			using (StreamWriter sw = new StreamWriter(file)){
				sw.WriteLine("installDir=" + installPath);
				sw.WriteLine("Arma2OA=" + armaPath);
			}
		}
		
		//read settings
		public static void readSettings(string file) {
			if(!File.Exists(file)) return;
			List<string> settings = new List<string>();
			//read lines in
			using(StreamReader sr = new StreamReader(file)) {
				while(sr.Peek() >= 0)
				{
					settings.Add(sr.ReadLine());
				}
			}
			//loop through and switch case
			settings.ForEach(delegate(string s) {
				//remove crap
				string t = s.Trim();
				//if key=value setting
				if(t.IndexOf("=") != -1) {
					//split string
					string[] args = t.Split('=');
					//do stuff
					switch(args[0]){
						case "installDir":
							installPath = args[1];
						break;
						case "Arma2OA":
							armaPath = args[1];
						break;
					}
				}
				//if single setting
				//else {
					//Console.WriteLine(t);
				//}
			});
		}
	}
}